
public class PortProvidedComponent implements ComponentProvidedInterface,PortComponent {

	private Component composant;
	private Attachement attachement;
	
	public PortProvidedComponent(Attachement attachement){}
	public PortProvidedComponent(Component composant){} // if we are in a binding link

	public void transmitMessage(String msg) {} // receive message and transmit to the attachment 
	public void transmitMessageComponent(String msg) {} // receive message and transmit to the component (this exist because of the binding)


}
