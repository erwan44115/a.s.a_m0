import java.util.List;

public class Configuration implements ArchitecturalElement {

	private List<ConfigurationProvidedInterface> cpiList;
	private List<ConfigurationRequiredInterface> criList;
	private List<ArchitecturalElement> aeList;
	
	public Configuration(Component cpt1, Component cpt2, Connector cnt1){} //a configuration contains at least two components and one connector linked together
	
	public void addPortProvided(PortProvidedConfiguration ppc) {} //add the provided port ppc to the configuration
	public void addPortRequired(PortRequiredConfiguration prc){} //add the required port prc to the configuration
	public void addElement(ArchitecturalElement ae){} //add a element ae to the configuration which can be a configuration, a component or a connector


}
