import java.util.List;

public class Component implements ArchitecturalElement {

	private List<ComponentProvidedInterface> providedIlist;
	private List<ComponentRequiredInterface> requiredIlist;
	private List<ArchitecturalElement> aeList;
	private ServiceRequired servicerequired;
	private ServiceProvided serviceprovided;
	
	public Component(PortProvidedComponent ppc, ServiceProvided sp) {} //a component exists only if it has a provided port and a provided service
	
	public void addPortProvided(PortProvidedComponent ppc){}
	public void addPortRequired(PortRequiredComponent prc){}
	public void addServiceProvided(ServiceProvided sp){}
	public void addServiceRequired(ServiceRequired sr){}
	public void addElement(ArchitecturalElement ae){}
	public void sendMsg(PortProvidedComponent port, String msg) {} // will call the service provided
	public void receiveMsg(PortRequiredComponent port, String msg){} // will call the service required
	public void setServiceRequired(ServiceRequired service) {}
	public void setServiceProvided(ServiceProvided service) {}
}
