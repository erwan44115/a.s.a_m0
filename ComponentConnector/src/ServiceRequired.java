
public class ServiceRequired implements ComponentRequiredInterface{

	private Component component;
	private PortRequiredComponent port;

	public ServiceRequired(Component component, PortRequiredComponent port) {}

	public void receiveMsg(PortRequiredComponent port, String msg){}
}
