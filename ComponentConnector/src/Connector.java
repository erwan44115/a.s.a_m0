import java.util.List;

public class Connector implements ArchitecturalElement {

	private List<ConnectorProvidedInterface> providedIlist;
	private List<ConnectorRequiredInterface> requiredIlist;
	private List<Glue> glueList;
	private List<Component> componentList;
	private boolean simpleConnector; //if true, than connector has at least one glue, else, than the connector is composite

	
	public Connector(boolean simpleconnector, RuleRequired rq, RuleProvided rp) {}
	
	public void addRuleProvided(RuleProvided rp){}
	public void addRuleRequired(RuleRequired rp){}
	public void addComponent(Component component){}
	public void addGlue(Glue glue){}
	

}
