
public class PortRequiredComponent implements ComponentRequiredInterface,PortComponent {
	private Component composant;
	private Binding binding;
	
	public PortRequiredComponent(Component composant){}
	public PortRequiredComponent(Binding binding){}
	
	public void transmitMessage(String msg) {} // receive message and transmit to the composant
	public void transmitMessageBinding(String msg) {}
}
