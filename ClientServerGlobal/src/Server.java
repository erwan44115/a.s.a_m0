
public class Server extends Component{

    public Server(PortProvidedServer pps, ServiceProvidedServer sps) {
        super(pps,sps);
    }

    public void sendMsg(PortProvidedServer port, String msg) {}
    public void receiveMsg(PortRequiredServer port, String msg){}
}
