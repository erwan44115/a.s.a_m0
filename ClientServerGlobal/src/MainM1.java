public class MainM1 {
    public static void main(String[] args){
        GlueFromClientToServer glueCS = new GlueFromClientToServer();
        GlueFromServerToClient glueSC = new GlueFromServerToClient();

        ServerCalling serverCalling = new ServerCalling(glueSC);

        AttachementFromServer attachementFromServer = new AttachementFromServer(serverCalling,"PortToRule");
        PortProvidedServer pps = new PortProvidedServer(attachementFromServer);
        ServiceProvidedServer sps = new ServiceProvidedServer(pps);
        Server server = new Server(pps,sps);
        PortRequiredServer prs = new PortRequiredServer(server);

        AttachementToServer attachementToServer = new AttachementToServer(prs,"RuleToPort");
        ServerCalled serverCalled = new ServerCalled(attachementToServer,glueCS);

        server.addPortRequired(prs);

        ClientCalling clientCalling = new ClientCalling(glueCS);

        AttachementFromClient attachementFromClient = new AttachementFromClient(clientCalling,"PortToRule");

        PortProvidedClient ppc = new PortProvidedClient(attachementFromClient);
        ServiceProvidedClient spc = new ServiceProvidedClient(ppc);
        Client client = new Client(ppc,spc);
        PortRequiredClient prc = new PortRequiredClient(client);
        client.addPortRequired(prc);
        AttachementToClient attachementToClient = new AttachementToClient(prc,"RuleToPort");

        ClientCalled clientCalled = new ClientCalled(attachementToClient,glueSC);
        RPC rpc = new RPC(true, serverCalling,clientCalled);
        glueCS.addConnector(rpc);
        glueSC.addConnector(rpc);

        glueCS.addRuleOut(serverCalled);
        glueCS.addRuleIn(clientCalling);

        glueSC.addRuleOut(clientCalled);
        glueSC.addRuleIn(serverCalling);
    }
}