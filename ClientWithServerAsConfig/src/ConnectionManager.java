
public class ConnectionManager extends Component{

    public ConnectionManager(Security_CheckProvided ppc, ServiceSecurityCheckProvided spc){
        super(ppc,spc);
    }
    public ConnectionManager(DB_QueryProvided pps, ServiceDBQueryProvided sps) {
        super(pps,sps);
    }

    public void sendMsg(Security_CheckProvided port, String msg) {}
    public void receiveMsg(Security_CheckRequired port, String msg){}

    public void sendMsg(DB_QueryProvided port, String msg) {}
    public void receiveMsg(DB_QueryRequired port, String msg){}

}
