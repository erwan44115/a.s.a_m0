
public class QueryConnector extends  Connector{

    public QueryConnector(boolean simpleconnector, DB_CalledBy_CM sc, CM_Calling_DB cc) {
        super(simpleconnector,sc,cc);
    }
    public QueryConnector(boolean simpleconnector, CM_CalledBy_DB cc, DB_Calling_CM sc) {
        super(simpleconnector,cc,sc);
    }

}
