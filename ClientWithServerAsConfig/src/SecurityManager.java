
public class SecurityManager extends Component{

    public SecurityManager(Security_AuthentificationProvided pps, ServiceSecurityAuthentificationProvided sps) {
        super(pps,sps);
    }
    public SecurityManager(Check_QueryProvided pps, ServiceCheckQueryProvided sps) {
        super(pps,sps);
    }

    public void sendMsg(Security_AuthentificationProvided port, String msg) {}
    public void receiveMsg(Security_AuthentificationRequired port, String msg){}

    public void sendMsg(Check_QueryProvided port, String msg) {}
    public void receiveMsg(Check_QueryRequired port, String msg){}

}
