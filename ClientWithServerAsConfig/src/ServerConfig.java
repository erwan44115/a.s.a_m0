
public class ServerConfig extends Configuration{

    public ServerConfig(ConnectionManager connectionManager, SecurityManager securityManager, ManagerConnector managerConnector){
        super(connectionManager, securityManager, managerConnector);
    }

    public ServerConfig(Database database, SecurityManager securityManager, SecurityConnector securityConnector){
        super(database, securityManager, securityConnector);
    }

    public ServerConfig(Database database, ConnectionManager connectionManager, QueryConnector queryConnector){
        super(database, connectionManager, queryConnector);
    }
}
