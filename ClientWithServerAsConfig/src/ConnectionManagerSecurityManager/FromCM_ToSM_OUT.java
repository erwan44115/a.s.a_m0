
public class FromCM_ToSM_OUT extends Attachement{

    public FromCM_ToSM_OUT(Security_AuthentificationRequired port, String sens) {
        super(port,sens);
    }
    public FromCM_ToSM_OUT(SM_Calling_CM rule, String sens) {
        super(rule,sens);
    }
}
