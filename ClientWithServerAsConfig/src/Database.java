
public class Database extends Component{

    public Database(Query_InterrogationProvided ppc, ServiceQueryInterrogationProvided spc){
        super(ppc,spc);
    }
    public Database(SecurityManagementProvided ppc, ServiceSecurityManagementProvided spc){
        super(ppc,spc);
    }

    public void sendMsg(Query_InterrogationProvided port, String msg) {}
    public void receiveMsg(Query_InterrogationRequired port, String msg){}

    public void sendMsg(SecurityManagementProvided port, String msg) {}
    public void receiveMsg(SecurityManagementRequired port, String msg){}

}
